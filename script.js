"use strict";

function filterBy(array, type) {
  return array.filter((item) => {
    if (typeof item === type) {
      return item;
    }
  })
}

const array1 = ['hello', 'world', 23, '23', null, 24, 42];
const filterArray = filterBy(array1, "number");
console.log(filterArray);